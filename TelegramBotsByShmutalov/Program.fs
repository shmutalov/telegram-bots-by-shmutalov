﻿module Program

open System
open Funogram.Bot
open Funogram.Api
open ExtCore.Control
open System.Threading
open Funogram.Types
open System.Text

let userName (user:Option<User>) = 
    match user with
    | Some user -> 
        match user.Username with 
        | Some nick -> sprintf "%s@%s" user.FirstName nick
        | None -> user.FirstName
    | None -> "GLOBAL"

let deleteMessage config chatId messageId = 
    let req = deleteMessage chatId messageId 
    api config req
    |> Async.Ignore
    |> Async.Start

let getUserInfo config username = 
    let req = getChatByName username
    api config req
    |> Async.RunSynchronously

let processUpdate context = 
    context.Update.Message
    |> Option.iter (fun (message:Message) -> 
        message.Entities
        |> Option.iter (fun entities ->
            let badTypes = ["email"; "text_link"; "url"]

            if Seq.exists (fun (entity:MessageEntity) -> badTypes |> Seq.contains entity.Type) entities then
                let username = userName message.From
                deleteMessage context.Config message.Chat.Id message.MessageId
                printfn "Chat [%i] > User [%s] > Deleting the message with link %i " 
                        message.Chat.Id username message.MessageId
            else
                entities
                |> Seq.takeWhile (fun (entity:MessageEntity) ->
                    // check, if mention is not a channel/group name
                    if entity.Type = "mention" then
                        let username = userName message.From
                        let mention = 
                            match message.Text with
                            | Some text -> text.Substring((int)entity.Offset, (int)entity.Length)
                            | None -> ""
                        
                        // check user type
                        let isNotUser = 
                            match getUserInfo context.Config mention with
                            | Ok _ -> true
                            | Result.Error _ -> false
                        
                        if isNotUser then
                            deleteMessage context.Config message.Chat.Id message.MessageId
                            printfn "Chat [%i] > User [%s] > Deleting the message with mention %i " message.Chat.Id username message.MessageId
                            false
                        else true
                    else true)
                |> Seq.isEmpty
                |> ignore
        )
    )

[<EntryPoint>]
let main _ =
    printfn "Starting the bot..."

    let token = Environment.GetEnvironmentVariable "SAY_NO_TO_LINKS_BOT_TOKEN"
    let cts = new CancellationTokenSource()
    let ev = new ManualResetEventSlim(false)

    Console.OutputEncoding <- Encoding.UTF8

    // register interupt handlers
    AppDomain.CurrentDomain.ProcessExit.Add(fun _ ->
        printfn "Stopping the bot..."
        cts.Cancel()
        ev.Set() |> ignore)
    Console.CancelKeyPress.Add(fun e ->
        printfn "Stopping the bot..."
        cts.Cancel()
        ev.Set() |> ignore
        e.Cancel <- true)

    // run bot
    let task = (startBot {
        defaultConfig with
            Token = token
    } processUpdate None)
    Async.Start (task, cts.Token)

    printfn "Bot started..."

    if Environment.UserInteractive then
        printfn "Press Ctrl + C to stop the bot"

    ev.Wait() |> ignore

    printfn "Bot stopped"

    0
