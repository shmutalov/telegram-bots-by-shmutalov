# Telegram Bots Collection by @shmutalov
Telegram bots collection created and maintained by @shmutalov

# License

`Telegram Bots Collection by @shmutalov` is distributed under the terms of both the MIT license and the Apache License (Version 2.0).

See [`LICENSE-APACHE`](LICENSE-APACHE) and [`LICENSE-MIT`](LICENSE-MIT) for details.